FROM denoland/deno:alpine-1.28.3

RUN mkdir /workspace
WORKDIR /workspace

COPY bot.ts /workspace/bot.ts

RUN deno cache bot.ts

ENTRYPOINT [ "deno", "run", "--allow-net", "--allow-env", "--allow-read", "bot.ts" ]
