#! /usr/bin/env -S deno run --allow-net --allow-env --allow-read

// Load the .env configuration
import { config as loadConfig } from "https://deno.land/std@0.167.0/dotenv/mod.ts";
import {
    Bot,
    ButtonStyles,
    Channel,
    createBot,
    deleteMessage,
    editMessage,
    EventHandlers,
    GatewayIntents,
    getChannel,
    getMember,
    Interaction,
    InteractionResponseTypes,
    MessageComponentTypes,
    sendInteractionResponse,
    sendMessage,
    startBot,
    User,
} from "https://deno.land/x/discordeno@17.0.1/mod.ts";

type ApplicationConfig = {
    APP_URL: string;
    APP_BOT_KEY: string;
    DISCORD_TOKEN: string;
    DISCORD_CHANNEL: string;
};

const config: ApplicationConfig = (await loadConfig({
    example: ".env.example",
    safe: true,
})) as ApplicationConfig;

const delays = {
    // How many seconds should I wait before deleting the message if nobody collects it?
    delete: 600, // Up to 10 minutes
    // How many seconds should I wait before posting another item?
    interval: 60, // 1 minute
    // Up to how many seconds can I add upon the configured posting limit for randomness?
    jitter: 60, // Up to 1 minute
};

const intents =
    GatewayIntents.Guilds |
    GatewayIntents.GuildMembers |
    GatewayIntents.GuildMessages;

type Collectible = {
    id: number;
    emoji: string;
    customImage: string | null;
    name: string;
    description: string;
    points: number;
};

const fetchCollectibles = async (): Promise<Array<Collectible>> => {
    console.log("fetching collectibles");

    return await fetch(config.APP_URL + "/api/collectibles").then((response) => response.json());
};

const updatePlayerScore = async (
    collectible: string,
    user: User
): Promise<void> => {
    const getAvatar = (id: bigint | undefined): string => {
        return id
            ? `https://cdn.discordapp.com/avatars/${user.id}/${id.toString(16).substring(1)}.png`
            : "https://cdn.discordapp.com/embed/avatars/0.png";
    };

    await fetch(config.APP_URL + "/api/bot/update-score", {
        method: "post",
        headers: {
            Authorization: `Bot ${config.APP_BOT_KEY}`,
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            discordId: user.id.toString(),
            discordName: user.username,
            discordAvatar: getAvatar(user.avatar),
            collectibleId: collectible,
        }),
    });
};

const selectCollectible = (collectibles: Array<Collectible>): Collectible => {
    if (collectibles.length < 1) {
        throw "There are no collectibles to pick from!";
    }

    const max = Math.max.apply(
        null,
        collectibles.map((item) => Math.abs(item.points))
    );
    const duplicated = collectibles.flatMap((item) => [
        ...new Array(max + 1 - Math.abs(item.points)).fill(item),
    ]);

    return duplicated[Math.floor(Math.random() * duplicated.length)];
};

const pendingMessages: Set<string> = new Set();

const end = new Date(2022, 11, 24, 0, 0, 0);

const sendCollectiblesMessage = async (bot: Bot, channel: Channel) => {
    console.log("Sending message...");

    if (new Date().getTime() >= end.getTime()) {
        return;
    }

    try {
        const collectibles = await fetchCollectibles();
        const selected = selectCollectible(collectibles);

        const codepoint = selected.emoji.codePointAt(0)?.toString(16);
        const image = selected.customImage !== null 
            ? `https://i.imgur.com/${selected.customImage}.png`
            : `https://emojiapi.dev/api/v1/${codepoint}/64.png`;

        const points = `${selected.points} point${
            selected.points === 1 ? "" : "s"
        }`;

        const message = await sendMessage(bot, channel.id, {
            embeds: [
                {
                    color: 0x5865f2,
                    title: `${selected.name} (${points})`,
                    description: selected.description,
                    image: {
                        url: image,
                        width: 64,
                        height: 64,
                    },
                },
            ],
            components: [
                {
                    type: MessageComponentTypes.ActionRow,
                    components: [
                        {
                            type: MessageComponentTypes.Button,
                            style: ButtonStyles.Primary,
                            label: "Collect",
                            customId: `collect:${selected.id}`,
                        },
                    ],
                },
            ],
        });

        pendingMessages.add(message.id.toString());

        const cleanup = () => {
            if (pendingMessages.has(message.id.toString())) {
                deleteMessage(bot, channel.id, message.id);
            }
        };

        setTimeout(() => cleanup(), delays.delete * 1000);
    } catch (error) {
        console.error(error);
    }
};

const startMessageTimer = (bot: Bot, channel: Channel) => {
    sendCollectiblesMessage(bot, channel);
    setInterval(
        () =>
            setTimeout(
                () => sendCollectiblesMessage(bot, channel),
                Math.random() * delays.jitter * 1000
            ),
        delays.interval * 1000
    );
};

const handleCollectibleInteraction = async (
    bot: Bot,
    interaction: Interaction
) => {
    const message = interaction.message!;
    const member = await getMember(
        bot,
        interaction.guildId!,
        interaction.user.id
    );

    const name = member.nick ?? member.user!.username;

    await sendInteractionResponse(bot, interaction.id, interaction.token, {
        type: InteractionResponseTypes.DeferredUpdateMessage,
    });

    await editMessage(bot, message.channelId, message.id, {
        embeds: message.embeds.map((embed) => ({
            ...embed,
            color: 0x2f3136,
            thumbnail: embed.image,
            image: undefined
        })),
        components: [
            {
                type: MessageComponentTypes.ActionRow,
                components: [
                    {
                        type: MessageComponentTypes.Button,
                        style: ButtonStyles.Secondary,
                        disabled: true,
                        customId: "-",
                        label: `Collected by ${name}`,
                    },
                ],
            },
        ],
    });

    pendingMessages.delete(message.id.toString());

    const [_, id] = interaction.data?.customId?.split(":") ?? [];

    console.log(`${name} collected the item with ID of ${id}`);

    await updatePlayerScore(id, interaction.user);
};

const events: Partial<EventHandlers> = {
    ready: async (bot, payload) => {
        const channel = await getChannel(bot, config.DISCORD_CHANNEL);

        console.log(`Logged in as ${payload.user.username}`);
        console.log(`Starting sending messages to #${channel.name}`);

        startMessageTimer(bot, channel);
    },
    interactionCreate: async (bot, interaction) => {
        if (interaction.data?.customId?.startsWith("collect:")) {
            await handleCollectibleInteraction(bot, interaction);
        }
    },
    messageCreate: async (bot, message) => {
        if (message.content.includes("Thank you <@1047854123225202699>")) {
            await sendMessage(bot, message.channelId, {
                content: `Thank you too <@${message.authorId}> 💕`,
                messageReference: {
                    guildId: message.guildId,
                    channelId: message.channelId,
                    messageId: message.id,
                    failIfNotExists: false
                } 
            });
        }
    }
};

const client = createBot({
    token: config.DISCORD_TOKEN,
    intents,
    events,
});

await startBot(client);
